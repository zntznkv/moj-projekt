commit 140cad3800b5c277915e89b71d174c4d12b10053
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sat Oct 12 18:23:13 2019 +0200

    peryl update

commit 8ed76afa5c1e069d2465de72c2e33ed9860a2e1a
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Oct 6 19:10:19 2019 +0200

    peryl update

commit 5a6d0bc6b496f353767bda40d0329464d6ac99fa
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Fri Oct 4 15:48:34 2019 +0200

    rename project to peryl-sitegen

commit db5665849b23f035f0d65f8917ee3e57952e6799
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Thu Feb 14 15:29:33 2019 +0100

    no sw init on localhost

commit 02fe4641bad3a059da6f8cd75d0310837a158727
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Thu Feb 14 15:05:01 2019 +0100

    npm scripts update

commit 9e9b11b3996119a3b1042b4e4969dd94895d3adf
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Thu Feb 14 00:35:48 2019 +0100

    prest-lib update

commit 80243bb6ad078565ac3f9c7d4f5aba63333788ff
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Mon Feb 4 16:18:47 2019 +0100

    typescript upgrade, hsml type chcecking fix

commit 4e0c6107c0a227f77d3876ac7cae16104ef2ed1c
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Mon Feb 4 13:32:57 2019 +0100

    jsonml replaced by hsml from prest-lib

commit 8bc583aa7a1ab52c3389d99a8362025648a8fc88
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Jun 24 01:27:38 2018 +0200

    repo link fix

commit 7f1d9abdc315caa1d85e452b0d60b48827bbee62
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Jun 10 18:47:38 2018 +0200

    gitlab ci image change

commit 52e139d52ec733aa3626029b863e31dccf6ac38b
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Jun 10 13:06:50 2018 +0200

    gitlab pages fix

commit f7a3350263a2b6368dd005cf872ed391071d7b27
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Jun 10 13:01:21 2018 +0200

    gitlab migration

commit 0fc2e1bc94ab0993e0a8e2df982ca6de9b2e4d09
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Jun 10 13:01:05 2018 +0200

    editorconfig update

commit 7a88825af8055eaaaf51b2eba299584bab07bb2f
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Jun 3 14:59:15 2018 +0200

    pretty html switch by param

commit ec9bf713a86299f67df0b79c0d5fcd4daa94b6e2
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Fri Jun 1 01:44:01 2018 +0200

    variable names refactoring to be more expressive

commit ab8cb657d6d55c26797ffa9c22fb2c493be4c664
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Wed Mar 14 20:00:46 2018 +0100

    build project before watch on 'npm start'

commit 9155b3edf1bebdc49df0178296e8f1d63345bf81
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 22:09:17 2018 +0100

    components moved to component dir

commit 214cd3ff528fbc06c4f2540a00f6c0c8ae0d44b1
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 20:18:47 2018 +0100

    site title link to root url

commit 968303faad327a9c3f16189f245ad70ee41e3d6c
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 20:03:21 2018 +0100

    pwa decription changed

commit 0a806a9ef10458a8cacd88ef5e5721d784c878c4
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 14:47:51 2018 +0100

    menu mark active item

commit 9a817b080fa0340c45f3cb398e3ee25e21f4562e
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 14:41:37 2018 +0100

    components separated to files

commit b3446b4c6dd5e87efb0a47b4ab8ce61f088745ab
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 14:16:59 2018 +0100

    snackbar removed

commit 0a8261bad1aecaa508b15745cd6a3a4193698502
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sun Feb 4 14:16:31 2018 +0100

    icon changed

commit 4abacd18b05421ce685c499ef61be1755a77219c
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sat Feb 3 22:57:08 2018 +0100

    link to gh-pages demo add to readme

commit 28fb44202d680c230a19e38346a70166beba5f43
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sat Feb 3 22:52:25 2018 +0100

    repo link update

commit 224bdde5c76ad4b3eeea71eca537f21e30710f17
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sat Feb 3 22:47:07 2018 +0100

    tslint config

commit c6c5d0ea0bbaadd443cd4f7ecd0509ce78324763
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sat Feb 3 22:46:34 2018 +0100

    project description, readme

commit 1d8d05a61e07e1c921ffa0777f0cd715dc6b99cc
Author: Peter Rybar <pr.rybar@gmail.com>
Date:   Sat Feb 3 22:40:34 2018 +0100

    init
